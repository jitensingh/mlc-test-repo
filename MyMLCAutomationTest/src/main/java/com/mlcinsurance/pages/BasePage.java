package com.mlcinsurance.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

//Default URL for opening MLC home page

@DefaultUrl("https://www.mlcinsurance.com.au/")

/**
 * The BasePage class is created by extending Serenity PageObject class
 * BasePage class is extended by all other page objects
 * @author Administrator
 *
 */
public class BasePage extends PageObject {

	public HomePage navigateToHomePage() {
		open();
		waitForTextToAppear("Home");
		return this.switchToPage(HomePage.class);
	}
}
