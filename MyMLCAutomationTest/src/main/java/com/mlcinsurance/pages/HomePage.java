package com.mlcinsurance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

//import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.mlcinsurance.com.au/")

public class HomePage extends BasePage {

	private static final String SEARCH_BUTTON = "//*[@id=\"nav-onscreen\"]/button";
	private static final String SEARCH_FIELD = "//*[@id=\"q\"]";

	/**
	 * This method searches for 'Lifeview' string in the MLC Homepage search box
	 * 
	 * @return
	 */
	public SearchResultsPage searchPageString() {
		// waitForTextToAppear("Home");
		waitFor(SEARCH_BUTTON).find(By.xpath(SEARCH_BUTTON)).click();
		waitFor(SEARCH_FIELD).$(SEARCH_FIELD).type("Lifeview").sendKeys(Keys.ENTER);
		waitForTextToAppear("Filter results");

		// $(SEARCH_BUTTON).click();

		return this.switchToPage(SearchResultsPage.class);

	}

}
