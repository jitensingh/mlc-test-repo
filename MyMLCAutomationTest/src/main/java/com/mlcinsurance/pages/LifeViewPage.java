package com.mlcinsurance.pages;

public class LifeViewPage extends BasePage {

	private static final String BREADCRUMBS = "/html/body/div[1]/div/div[3]/div[1]/div/ul";
	private static final String REQUEST_A_DEMO_LINK = "//*[@id=\"main\"]/div[2]/p[2]/a/span";

	/**
	 * This method clicks on the book a demo button on the LifeView page
	 * 
	 * @return
	 */
	public RequestLifeViewDemoPage clickRequestDemoLink() {

		// waitFor(SEARCH_BUTTON).find(By.xpath(SEARCH_BUTTON )).click();
		waitFor(REQUEST_A_DEMO_LINK).$(REQUEST_A_DEMO_LINK).click();
		waitForTextToAppear("Request a LifeView demo");

		return this.switchToPage(RequestLifeViewDemoPage.class);

	}

	/**
	 * This method verifies if the bread-crumb is present in the LifeView page.
	 * 
	 * @return
	 */
	public boolean verifyBreadcrumbs() {

		return $(BREADCRUMBS).isDisplayed();

	}

}
