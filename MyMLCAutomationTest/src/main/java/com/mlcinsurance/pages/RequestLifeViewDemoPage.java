package com.mlcinsurance.pages;

public class RequestLifeViewDemoPage extends BasePage {

	private static final String NAME_INPUT = "//*[@id=\"wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_0__Value\"]";

	private static final String COMPANY_INPUT = "//*[@id=\"wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_1__Value\"]";

	private static final String EMAIL_INPUT = "//*[@id=\"wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_2__Value\"]";

	private static final String PHONE_INPUT = "//*[@id=\"wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_3__Value\"]";

	private static final String PREFFERED_TIME_INPUT = "//*[@id=\"wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_5__Value\"]";

	private static final String REQUEST_DETAILS_INPUT = "//*[@id=\"wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_6__Value\"]";

	/**
	 * This method fills up the user data into LifeView request demo form.
	 * 
	 * @param name
	 * @param company
	 * @param email
	 * @param phone
	 * @param prefferedTime
	 * @param requestDetails
	 * @return
	 */

	public RequestLifeViewDemoPage enterRequestALifeViewDemoDetails(String name, String company, String email,
			String phone, String prefferedTime, String requestDetails) {

		$(NAME_INPUT).type(name);
		$(COMPANY_INPUT).type(company);
		$(EMAIL_INPUT).type(email);
		$(PHONE_INPUT).type(phone);
		if ("AM".equalsIgnoreCase(prefferedTime)) {
			$(PREFFERED_TIME_INPUT).click();
		}

		$(REQUEST_DETAILS_INPUT).type(requestDetails);

		return this;

	}
}
