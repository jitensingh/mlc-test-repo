package com.mlcinsurance.pages;

import org.openqa.selenium.By;

public class SearchResultsPage extends BasePage {

	private static final String LIFEVIEW_LINK = "//*[@id=\"main\"]/section/ol/li[1]/div/a/div/h2";

	/**
	 * This method clicks on the LifeView link on search result page
	 * 
	 * @return
	 */
	public LifeViewPage clickLifeViewLink() {
		// waitForTextToAppear("Filter results");
		waitFor(LIFEVIEW_LINK).find(By.xpath(LIFEVIEW_LINK)).click();
		waitForTextToAppear("MLC Online");
		// $(SEARCH_BUTTON).click();
		return this.switchToPage(LifeViewPage.class);

	}

}
