package com.mlcinsurance.cucumber.steps;

import com.mlcinsurance.steps.MLCInsuranceSteps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Feature file statements steps definitions or bindings
 * 
 * @author Administrator
 *
 */
public class LifeViewDemoBookingSteps {

	@Steps
	MLCInsuranceSteps customer;

	@Given("I navigate to MLC life insurance home page")
	public void Navigate_MLCLifeInsurance_HomePage() {
		customer.navigateToHomePage();
	}

	@And("On home page search for LifeView page using search functionality")
	public void Search_LifeViewPage_Using_Search_Functionality() {
		customer.lifeViewPageSearch();

	}

	@Then("I navigate to search results page where I click on LifeView link")
	public void Navigate_SearchResultsPage_Click_LifeView_Link() {
		customer.clickLifeViewLinkOnSearchResultsPage();

	}

	@When("I navigate to LifeView page I can see breadcrumbs on top of it")
	public void Navigate_LifeViewPage_Verify_Breadcrumbs() {
		customer.verifyBreadcrumbsDisplayedOnLifeViewPage();
	}

	@And("I click on Request a Demo link on LifeView page")
	public void Click_RequestADemoLink_On_LifeView_page() {
		customer.clickOnRequestDemoLinkOnLifeViewPage();
	}

	@Then("I am able to enter required data into request a life demo form")
	public void Enter_Required_data_Into_Form() {
		customer.enterRequiredDataIntoRequestALifeDemoForm("Jiten", "Computershare", "jscqu@yahoo.com.au", "0402510437",
				"AM", "MLC Automation Engineer Test");
	}

}
