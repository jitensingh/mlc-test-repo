package com.mlcinsurance.steps;

import org.openqa.selenium.By;

import com.mlcinsurance.pages.BasePage;
import com.mlcinsurance.pages.HomePage;
import com.mlcinsurance.pages.LifeViewPage;
import com.mlcinsurance.pages.RequestLifeViewDemoPage;
import com.mlcinsurance.pages.SearchResultsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class MLCInsuranceSteps extends ScenarioSteps {
	BasePage basePage;
	HomePage homePage;
	LifeViewPage lifeViewPage;
	SearchResultsPage searchResultsPage;
	RequestLifeViewDemoPage requestLifeViewDemoPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * BASE PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("Navigate to MLC life insurance home page")
	public HomePage navigateToHomePage() {
		return basePage.navigateToHomePage();
	}

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * HOME PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("On home page search for LifeView page using search functionality")
	public SearchResultsPage lifeViewPageSearch() {
		return homePage.searchPageString();

	}

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * SEARCH RESULTS PAGE
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("Click on LifeView link on search results page")
	public LifeViewPage clickLifeViewLinkOnSearchResultsPage() {
		return searchResultsPage.clickLifeViewLink();
	}

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * LIFE VIEW PAGE
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("Click on  Request a Demo link on lifeview page")
	public RequestLifeViewDemoPage clickOnRequestDemoLinkOnLifeViewPage() {
		return lifeViewPage.clickRequestDemoLink();
	}

	@Step("Verify that breadcrumbs are dispayed on the life view page")
	public boolean verifyBreadcrumbsDisplayedOnLifeViewPage() {
		return lifeViewPage.verifyBreadcrumbs();
	}

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * RUEST LIFEVIEW DEMO PAGE
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("Enter required data into request a life demo form")
	public void enterRequiredDataIntoRequestALifeDemoForm(String name, String company, String email, String phone,
			String prefferedTime, String requestDetails) {
		requestLifeViewDemoPage.enterRequestALifeViewDemoDetails(name, company, email, phone, prefferedTime,
				requestDetails);

	}
}
