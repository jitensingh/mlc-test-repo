package com.mlcinsurance.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.mlcinsurance.steps.MLCInsuranceSteps;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

/**
 * This is Junit test. This method also runs the tests mentioned in the
 * assessment-1 Initially I implemented using Junit, but later it was integrated
 * with Cucumber.
 */

@RunWith(SerenityRunner.class)
public class RequestForLifeViewDemoTest {

	/**
	 * This method executes each steps one by one for the test mentioned in
	 * assessment-1
	 */
	@Managed
	WebDriver driver;

	@Steps
	MLCInsuranceSteps enterLifeViewFormData;

	@Test
	@Title("Verify if a user can fill LifeView form successfully")

	public void verifyIfFormDataCanBeFilledSuccessfully() throws InterruptedException {
		enterLifeViewFormData.navigateToHomePage();
		enterLifeViewFormData.lifeViewPageSearch();
		enterLifeViewFormData.clickLifeViewLinkOnSearchResultsPage();
		enterLifeViewFormData.clickOnRequestDemoLinkOnLifeViewPage();
		boolean ifCreadcrumsExists = enterLifeViewFormData.verifyBreadcrumbsDisplayedOnLifeViewPage();
		assertEquals(true, ifCreadcrumsExists);
		enterLifeViewFormData.enterRequiredDataIntoRequestALifeDemoForm("Jiten", "Computershare", "jscqu@yahoo.com.au",
				"0402510437", "AM", "MLC Automation Engineer Test");
		Thread.sleep(3000);

	}
}
