Feature: User booking a LifeView platform demo 

Scenario: As a user, I want to book a LifeView digital platform demo for making insuarnace and claims easier 
	Given I navigate to MLC life insurance home page 
	And On home page search for LifeView page using search functionality
	Then I navigate to search results page where I click on LifeView link 
	When I navigate to LifeView page I can see breadcrumbs on top of it
	And I click on Request a Demo link on LifeView page 
	Then I am able to enter required data into request a life demo form
