# MLC Automation Test

MyMLCAutomationTest project is created for MLC screening test. A UI automation framework is created using Serenity BDD framework, Cucumber, Selenium, Maven and
JUnit by using  Page Object Model(POM) pattern and Eclipse IDE. The project is created using JDK 1.8 and Eclipse 4.11.

A feature file 'lifeviewdemo-booking.feature' is created for the test mentioned in Part 1 of the assessment. 

# How to run the tests
The scenario withnin the feature file can be executed using Cucumber Runner by creating a maven Run Configuration by setting the goal as 'clean verify serenity:aggregate'. Once the maven
build is succesfully done, then the scenario mentioned in the feature file runs and the test reports are generated. 

Note: This test can also be ran as standalone Junit test by running the class 'com.mlcinsurance.test.RequestForLifeViewDemoTest'.


# Test Reports
The test reports can be viewed by launching the 'target/site/serenity/index.html' in a browser. The generated report is a single-page, self-contained HTML summary report, 
containing an overview of the test results.